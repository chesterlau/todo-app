var express = require('express');
var path = require('path');
var app = express();

app.use(express.static(path.join(__dirname)));

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

//socket io
const io = require('socket.io')();
app.io = io;

io.on('connection', function(socket){
  console.log(socket.id);

  socket.on('LOAD_TODOS', function(data) {
    //console.log(data);
    io.emit('SET_TODOS', data.todos);
  });

});

module.exports = app;