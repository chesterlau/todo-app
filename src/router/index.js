import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/views/Home';
import NewTodo from '@/views/NewTodo';
import DisplayTodo from '@/views/DisplayTodo';

Vue.use(Router);

export default new Router ({
  
  routes: [
    {
      path: '/',
      component: Home,
      meta: {
        title: 'Home'
      }
    },
    {
      path: '/NewTodo',
      component: NewTodo,
      meta: {
        title: 'New Todo'
      }
    },
    {
      path: '/DisplayTodo',
      component: DisplayTodo,
      meta: {
        title: 'Display Todo'
      }
    }
  ],
  //mode: 'history',
  linkActiveClass: "active"

});