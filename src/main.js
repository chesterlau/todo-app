import Vue from 'vue';
import App from './App.vue';
import router from '@/router';
import { store } from '@/store/store';
import Toastr from 'vue-toastr';

//Change the title of the html pages depending on the routes
router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  next()
})

//Use toastr plugin
Vue.use(Toastr);

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
