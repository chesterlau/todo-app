import axios from 'axios';
const apiUrl = 'https://76zz1dc2uk.execute-api.ap-southeast-2.amazonaws.com/dev/todos';

export default {

  getTodos() {
    return axios.get(apiUrl);
  },
  addTodo(message) {
    return axios.post(apiUrl, {'text': message});
  },
  deleteTodo(id) {
    return axios.delete(`${apiUrl}/${id}`);
  }

}