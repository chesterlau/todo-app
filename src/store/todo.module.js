import _ from 'lodash';
import TodoService from '@/services/TodoService';
import io from 'socket.io-client';

const state = {
  todos: [],
  socket: io()
}
const getters = {
  getTodos(state) {
    return _.sortBy(state.todos, 'updatedAt');
  },
}
const mutations = {
  setTodos(state, todos) {
    state.todos = todos;

    //TODO: move the emit to new todo action
    state.socket.emit('LOAD_TODOS', {
      todos
    });

    state.socket.on('SET_TODOS', data => {
      state.todos = data
    });

  }
}

const actions = {
  loadTodos({ commit }) {
    TodoService.getTodos()
      .then(response => {
        commit('setTodos', response.data);
      })
      .catch(error => {
        console.log(error);
      });
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
